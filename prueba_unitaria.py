import unittest
from clase_coche import Coche

class TestCoche(unittest.TestCase):

    def test_acelerar(self):
        coche1 = Coche("gris", "citroën", "c3", "1234 BCD", 100)
        acelerar = coche1.acelerar()

        self.assertEqual(acelerar.velocidad, 101)
       
    
    def test_frenar(self):
        coche1 = Coche("gris", "citroën", "c3", "1234 BCD", 100)
        frenar = coche1.frenar()

        self.assertEqual(frenar.velocidad, 99)
if __name__ == '__main__':
    unittest.main()