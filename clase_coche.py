class Coche:
    def __init__(self, c, m, mod, matr, v = 0):
        self.color = c
        self.marca = m
        self.modelo = mod
        self.matricula = matr
        self.velocidad = v
    
    def acelerar(self):
        self.velocidad = self.velocidad + 1
        return self
    
    def frenar(self):
        self.velocidad = self.velocidad - 1
        return self
    
    def __str__(self):
        return f"Tu coche es de color {self.color} de la marca {self.marca}, es el modelo {self.modelo} con matricula {self.matricula} y va a {self.velocidad} km/h"
    
coche01 = Coche("gris", "citroën", "c3", "1234 BCD", 103)
print(coche01)

coche01.acelerar()
print(coche01.velocidad)
coche01.frenar()
print(coche01.velocidad)